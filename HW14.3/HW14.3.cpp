// HW14.3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

int main()
{
    
    std::cout << "5 + 5 = ";
    std::string value;
    std::getline(std::cin, value);


    std::cout << value.length() << "\n";
    std::cout << value.front() << "\n";
    std::cout << value.back() << "\n";


    std::cout << value << " " << "\n";

}

